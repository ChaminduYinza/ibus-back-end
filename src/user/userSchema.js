//import validator class
const joi = require('joi');

//user login Schema and validations to be done 
module.exports.login = joi.object().keys({
    email: joi.string().email().required(),
    password: joi.required() 
});

//user registration Schema and validations to be done 
module.exports.newUser = joi.object().keys({
    email: joi.string().email().required(),
    role: joi.number().integer().min(1).max(10).required(),
    password: joi.required(),
    busNo :joi.string(),
    route:joi.string(),
    capacity:joi.number(),
});
//user password change Schema and validations to be done 
module.exports.changePassword = joi.object().keys({
    email: joi.string().email().required(),
    oldPassword: joi.required(),
    newPassword: joi.required()
});

//VA user registration Schema and validations to be done 
module.exports.vaUserCreation = joi.object().keys({
    email: joi.string().email().required(),
    role: joi.number().integer().min(1).max(10).required(),
});

//user role Schema and validations to be done 
module.exports.role = joi.object().keys({
    role: joi.number().required(),
});

//user_id Schema and validations to be done 
module.exports.user_id = joi.object().keys({
    user_id: joi.string().alphanum().min(24).max(24).required(),
});
