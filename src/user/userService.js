'use strict'
//Imports
const modelUser = require('./userModel');
const callBackResponse = require('../../services/callBackResponseService');
var generatePassword = require('password-generator');

//add new user
module.exports.newUser = function (req, res, callBack) {
    const user = new modelUser();
    user.email = req.body.email;
    user.role = req.body.role;
    user.busNo = req.body.busNo;
    user.milage = req.body.milage;
    user.route = req.body.route;
    user.capacity = req.body.capacity;
    user.setPassword(req.body.password);
    //check user already registred
    modelUser.findOne({ email: req.body.email }, function (error, UserRet) {
        if (error) {
            callBack(callBackResponse.callbackWithDefaultError());
        }
        else if (UserRet == null || UserRet == undefined) {
            //add user to the database
            user.save(function (err) {
                if (err) {
                    callBack(callBackResponse.callbackWithDefaultError());
                } else {
                    callBack(callBackResponse.callbackWithSucessMessage('Registration successful'));
                }
            });
        } else {
            callBack(callBackResponse.callbackWithfalseMessage('Email Already Exist'));
        }
    });
}


/**
 * user login to the system 
 */
module.exports.login = function (req, res, callBack) {
    //find user from the database using email
    modelUser.findOne({ email: req.body.email }, function (err, user) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        }
        else if (user == null || user == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Invalid user Name'));
        }
        else {
            const status = user.validPassword(req.body.password);
            if (!status) {
                callBack(callBackResponse.callbackWithfalseMessage('Invalid Password'));
            } else {
                const data = { role: user.role, email: user.email, id: user._id , busNo: user.busNo, route: user.route , capacity: user.capacity , milage: user.milage};
                callBack(callBackResponse.callbackWithData(data));
            }
        }
    });
}

/**
 * change current password of the user
 */
module.exports.changePassword = function (req, res, callBack) {
    //find user from the database using email
    modelUser.findOne({ email: req.body.email }, function (err, user) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        }
        else if (user == null || user == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Invalid user Name'));
        }
        else {
            const status = user.validPassword(req.body.oldPassword);
            if (!status) {
                callBack(callBackResponse.callbackWithfalseMessage('Current Password is Incorrect'));
            }
            user.setPassword(req.body.newPassword);
            user.save(function (err) {
                if (err) {
                    callBack(callBackResponse.callbackWithDefaultError());
                } else {
                    callBack(callBackResponse.callbackWithSucessMessage('sucessfully changed the password'));
                }
            });
        }
    });
}

/**
 * find user by user ID
 */
module.exports.getUserByID = function (user_id, res, callBack) {
    //find users from the database using role
    modelUser.findById(user_id, function (err, user) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        }
        else if (user == null || user == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('Invalid User ID'));
        }
        else {
            const data = { role: user.role, email: user.email, id: user._id , busNo: user.busNo, route: user.route , capacity: user.capacity , milage: user.milage};
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

//delete User from the database
module.exports.removeUser = function (user_id, res, callBack) {
    //check User is on the database
    modelUser.findById(user_id, function (error, user) {
        if (error) {
            callBack(callBackResponse.callbackWithDefaultError());
        }
        else if (user == null || user == undefined) {
            callBack(callBackResponse.callbackWithfalseMessage('User Not Found'));
        } else {
            //delete User from the database
            modelUser.remove({ _id: user._id }, function (err) {
                if (err) {
                    callBack(callBackResponse.callbackWithDefaultError());
                } else {
                    callBack(callBackResponse.callbackWithSucessMessage('User Removed From The System'));
                }
            });
        }
    });
};