'use strict'
//Imports
const locationDataModel = require('./locationDataModel');
const callBackResponse = require('../../../services/callBackResponseService');

//add new cordinate
module.exports.newCordinate = function (req, res, callBack) {
    const cordinate = new locationDataModel();
    cordinate.longitude = req.body.longitude;
    cordinate.latitude = req.body.latitude;
    cordinate.bus_id = req.body.bus_id;
    //add cordinate to the database
    cordinate.save(function (err) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithSucessMessage('cordinate saved'));
        }
    });
}

//get cordinates from the database
module.exports.getRawCordinates = function (body, res, callBack) {
    locationDataModel.find({ bus_id: body.bus_id }, function (error, bus) {
        if (error) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (bus != undefined || bus != null || bus.length != 0){
            locationDataModel.find({ bus_id: body.bus_id, updatedAt: { $lte: body.startDate } }, function (err, data) {
                if (err) {
                    callBack(callBackResponse.callbackWithDefaultError());
                } else {
                    callBack(callBackResponse.callbackWithData(data));
                }
            });
        }else{
            callBack(callBackResponse.callbackWithfalseMessage("bus_id not found"));
        }
    });
}