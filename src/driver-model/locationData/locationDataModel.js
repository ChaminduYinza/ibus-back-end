const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user Schema 
const locationRawData = new Schema({
  longitude: {
    type: String,
    required: true,
  },
  latitude:{
    type: String,
    required: true
  },
  bus_id:{
    type: String,
    required: true
  },
}, {timestamps: true});

module.exports = mongoose.model('busCordinates', locationRawData);