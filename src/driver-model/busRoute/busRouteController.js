'use strict'
//Import Required modules
const routeService = require('./busRouteService');
const response = require('../../../services/responseService');

/**
 * add new bus route to the system
 * @param {*} req route_no , total_km , bus_stops , allocated_time 
 * @param {*} res 
 */
module.exports.newBusRoute = function (req, res) {
    routeService.AddNewRoute(req.body,res,function(data){
        if(data.status){
            return response.successWithData(data.data,res)
        }else{
            return response.customError(data.data,res);
        }
    });
}

/**
 * edit bus route from the database
 * @param {*} req  route_id , route_no , total_km , bus_stops , allocated_time 
 * @param {*} res 
 */
module.exports.updateRoute = function (req, res) {
    routeService.updateRoutes(req.body,res,function(data){
        if(data.status){
            return response.successWithMessage(data.data,res)
        }else{
            return response.customError(data.data,res);
        }
    });
}

/**
 * get all bus route from the datasbase
 * @param {*} req 
 * @param {*} res 
 */
module.exports.getAllRoutes = function (req, res) {
    routeService.getAllRoutes(req.body,res,function(data){
        if(data.status){
            return response.successWithMessage(data.data,res)
        }else{
            return response.customError(data.data,res);
        }
    });
}

/**
 * get all bus route from the datasbase according to the bus route_id
 * @param {*} req route_id
 * @param {*} res 
 */
module.exports.filterById = function (req, res) {
    routeService.filterById(req.body,res,function(data){
        if(data.status){
            return response.successWithMessage(data.data,res)
        }else{
            return response.customError(data.data,res);
        }
    });
}

/**
 * get all bus route from the datasbase according to the bus route_no
 * @param {*} req route_no
 * @param {*} res 
 */
module.exports.filterByRouteNo = function (req, res) {
    routeService.filterByRoute(req.body,res,function(data){
        if(data.status){
            return response.successWithMessage(data.data,res)
        }else{
            return response.customError(data.data,res);
        }
    });
}