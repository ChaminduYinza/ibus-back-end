'use strict'
//Imports
const busRouteServiceModel = require('./busRouteModel');
const callBackResponse = require('../../../services/callBackResponseService');

//add new cordinate
module.exports.AddNewRoute = function (body, res, callBack) {
    const route = new busRouteServiceModel();
    route.route_no = body.route_no;
    route.total_km = body.total_km;
    route.bus_stops = body.bus_stops;
    route.allocated_time = body.allocated_time;
    //add new routes to the database
    route.save(function (err,data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

//update routes from the database 
module.exports.updateRoutes = function (body, res, callBack) {
    busRouteServiceModel.findById(body.route_id, function (error, route) {
        if (error) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (route != undefined || route != null ) {
            var object = {};
            object.route_no = body.route_no;
            object.total_km = body.total_km;
            object.bus_stops = body.bus_stops;
            object.allocated_time = body.allocated_time;
            busRouteServiceModel.findOneAndUpdate({_id:body.tracking_id},object,{ "new": true},function(err,track){
                if (err) {
                    callBack(callBackResponse.callbackWithDefaultError());
                } else {
                    callBack(callBackResponse.callbackWithSucessMessage(track));
                }
            })
        } else {
            callBack(callBackResponse.callbackWithfalseMessage("Route not found"));
        }
    });
}

//get All Routes from the database according to the route
module.exports.getAllRoutes = function (body, res, callBack) {
    //find Routes from database
    trackingModel.find({},function (err,data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

//get All tracker from the database 
module.exports.filterByRoute = function (body, res, callBack) {
    //find tracker from database and Return
    trackingModel.find({route_no:body.route_no },function (err,data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

//find route using route_id
module.exports.filterById = function (body, res, callBack) {
    //find route from database and Return
    trackingModel.findById(body.route_id,function (err,data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}