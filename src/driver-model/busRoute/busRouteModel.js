const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create Bus ROute Schema 
const busRouteSchema = new Schema({
  route_no:String,
  total_km:String,
  bus_stops:Schema.Types.Mixed,
  allocated_time:String
},{timestamps:true});

module.exports = mongoose.model('busRoute', busRouteSchema);