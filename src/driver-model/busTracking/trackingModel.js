const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user Schema 
const busTrackingSchema = new Schema({
  longitude:String,
  latitude:String,
  bus:Schema.Types.Mixed,
  status:String
},{timestamps:true});

module.exports = mongoose.model('busTracker', busTrackingSchema);