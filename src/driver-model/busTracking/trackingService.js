'use strict'
//Imports
const trackingModel = require('./trackingModel');
const callBackResponse = require('../../../services/callBackResponseService');

//add new cordinate
module.exports.startTracking = function (body, res, callBack) {
    const tracker = new trackingModel();
    tracker.longitude = body.longitude;
    tracker.latitude = body.latitude;
    tracker.bus = body.bus;
    tracker.status = body.status;
    //add cordinate to the database
    tracker.save(function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

//get cordinates from the database
module.exports.trackingUpdate = function (body, res, callBack) {
    trackingModel.findById(body.tracking_id, function (error, tracker) {
        if (error) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (tracker != undefined || tracker != null) {
            var object = {};
            object.longitude = body.longitude;
            object.latitude = body.latitude;
            object.status = body.status;
            trackingModel.findOneAndUpdate({ _id: body.tracking_id }, object, { "new": true }, function (err, track) {
                if (err) {
                    callBack(callBackResponse.callbackWithDefaultError());
                } else {
                    callBack(callBackResponse.callbackWithSucessMessage(track));
                }
            })
        } else {
            callBack(callBackResponse.callbackWithfalseMessage("tracker not found"));
        }
    });
}

//get All trackers from the database according to the route
module.exports.getActiveTrackerForRoute = function (body, res, callBack) {
    //find trackers from database
    trackingModel.find({ status: body.status, 'bus.route': body.route }, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

//get All trackers from the database 
module.exports.getActiveTrackers = function (body, res, callBack) {
    //find trackers from database
    trackingModel.find({ status: body.status }, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}

//check weather the bus has active tracker
module.exports.checkActiveTracker = function (body, res, callBack) {
    //find trackers from database
    trackingModel.findOne({ status: body.status, 'bus.id': body.bus_id }, function (err, data) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (data == null || data == undefined || data.length == 0) {
            callBack(callBackResponse.callbackWithfalseMessage("No active trackers"));
        } else {
            callBack(callBackResponse.callbackWithData(data));
        }
    });
}