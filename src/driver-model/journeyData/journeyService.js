'use strict'
//Imports
const journeyModel = require('./journeyModel');
const locationDataSerice = require('../locationData/locationDataService');
const callBackResponse = require('../../../services/callBackResponseService');

//add new cordinate
module.exports.startJourney = function (body, res, callBack) {
    const journey = new journeyModel();
    journey.startTime = body.startTime;
    journey.bus = body.bus;
    journey.tracker = body.tracker;
    //add cordinate to the database
    journey.save(function (err, object) {
        if (err) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else {
            callBack(callBackResponse.callbackWithData(object));
        }
    });
}

//get cordinates from the database
module.exports.journeyEnd = function (body, res, callBack) {
    journeyModel.findOne({ 'bus.id': body.bus_id, tracker: body.tracker }).lean().exec(function (error, journey) {
        if (error) {
            callBack(callBackResponse.callbackWithDefaultError());
        } else if (journey != undefined || journey != null) {
            var totalTime = timeDifference(journey.startTime, body.endTime);
            var KMPH = calculeteKMPH(journey.bus.milage, totalTime);
            journey.endTime = body.endTime;
            journey.speed = KMPH;
            journey.totalTime = totalTime;
            journeyModel.findOneAndUpdate({ _id: journey._id }, journey, { new: true }, function (err, data) {
                if (err) {
                    callBack(callBackResponse.callbackWithDefaultError());
                } else {
                    callBack(callBackResponse.callbackWithData(data));
                }
            });
        } else {
            callBack(callBackResponse.callbackWithfalseMessage("journey not found"));
        }
    });
}

//get the time difference between tracking start time and end time
function timeDifference(startTime, endTime) {
    var startTime = new Date(parseInt(startTime));
    var endTime = new Date(parseInt(endTime));
    var timeDiff = Math.round(Math.abs((startTime.getTime() - endTime.getTime()) / 1000));
    return timeDiff;
}

//calculte the KMPH
function calculeteKMPH(distance, timeinSeconds) {
    var MPS = distance / timeinSeconds;
    var KMPH = (MPS * 18) / 5;
    return KMPH;
}