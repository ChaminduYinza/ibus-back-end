const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user Schema 
const locationRawData = new Schema({
  startTime:String,
  endTime:String,
  speed:String,
  bus:Schema.Types.Mixed,
  totalTime:String,
  tracker:Schema.Types.Mixed,
});

module.exports = mongoose.model('journeyStatus', locationRawData);