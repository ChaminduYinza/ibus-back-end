'use strict'
//Import Express
const express = require('express');
//user router
const router = express.Router();
//Import body parser
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended:true}));
router.use(bodyParser.json());

//import  controllers
const userController = require('../src/user/usersController');
const locationDataController = require('../src/driver-model/locationData/locationDataController');
const journeyController = require('../src/driver-model/journeyData/journeyController');
const trackingController = require('../src/driver-model/busTracking/trackingController');
const busRouteController = require('../src/driver-model/busRoute/busRouteController');

//import Validator class
const validator = require('../validators/validator');

//import validator Schemas 
const  userSchema = require ('../src/user/userSchema');
const  busRouteSchema = require('../src/driver-model/busRoute/busRouteSchema');

//user routes
router.route('/api/user/new').post(validator.validateBody(userSchema.newUser),userController.newUser);
router.route('/api/user/login').post(validator.validateBody(userSchema.login),userController.login);
router.route('/api/user/changePassword').put(validator.validateBodyWithToken(userSchema.changePassword),userController.changePassword);
router.route('/api/user/remove').post(validator.validateBodyWithToken(userSchema.user_id),userController.RemoveUser);
router.route('/api/user/getUser').post(validator.validateBodyWithToken(userSchema.user_id),userController.getUserById);

//locationData routes
router.route('/api/cordinate/new').post(locationDataController.newCordinate);
router.route('/api/cordinate/getRawData').post(locationDataController.getRawCordinates);

//tracking routes
router.route('/api/tracking/new').post(trackingController.newTracking);
router.route('/api/tracking/update').post(trackingController.updateTracker);
router.route('/api/tracking/get').post(trackingController.getTrackers);
router.route('/api/tracking/route').post(trackingController.getRouteActiveTrackers);

//journey routes
router.route('/api/journey/create').post(journeyController.startJourney);
router.route('/api/journey/end').post(journeyController.journeyEnd);
router.route('/api/journey/get').post(journeyController.journeyEnd);

//busRoute route
router.route('/api/route/new').post(validator.validateBodyWithToken(busRouteSchema.newRoute),busRouteController.newBusRoute);
router.route('/api/route/edit').post(validator.validateBodyWithToken(busRouteSchema.editRoute),busRouteController.updateRoute);
router.route('/api/route/filterById').post(validator.validateBodyWithToken(busRouteSchema.route_id),busRouteController.filterById);
router.route('/api/route/filterById').post(validator.validateBodyWithToken(busRouteSchema.route_no),busRouteController.filterByRouteNo);
router.route('/api/route/getAll').get(busRouteController.getAllRoutes);

module.exports = router;