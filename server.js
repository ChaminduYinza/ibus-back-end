'use strict'

//Import cross origins
const cors = require('cors');
//Import express
const express = require('express');
//Import Configurations 
const config = require('./config/config');
//import controllers js
const routes = require('./routes');
//import MongoDB
var mongoose = require('mongoose');

var socketServer = require('http').createServer();

const server = express();
//Allow cross origins
server.use(cors());
//Set constant server port
const server_port = config.web_port;
//set routes
server.use(routes);
server.use(express.static(__dirname));

//Database Connection initiation
mongoose.connect('mongodb:' + config.database);
var database = mongoose.connection;

server.get('/', (req, res, next) => {
    console.log("Error !!!");
});


let app = require('express')();
let http = require('http').Server(server);
let io = require('socket.io')(http);
const trackingService = require('./src/driver-model/busTracking/trackingService');
const journeyService = require('./src/driver-model/journeyData/journeyService');
const locationService = require('./src/driver-model/locationData/locationDataService');

io.on('connection', (socket) => {
    console.log('user connected');

    socket.on('disconnect', function () {
        console.log('user disconnected');
    });

    socket.on('connect', function (busId) {
        console.log('user disconnected');
    });

    socket.on('location', (locationObj) => {
        var obj = {};
        obj.status = "Active";
        obj.bus_id = locationObj.bus.id;
        trackingService.checkActiveTracker(obj, "res", (data) => {
            //     console.log(data.status)
            //     console.log(data)
            if (locationObj.status == "Deactive") {
                journeyService.journeyEnd({ bus_id: locationObj.bus.id, tracker: data.data._id, endTime: locationObj.endTime }, "", (journey) => {
                    console.log(journey);
                    var location = {};
                    location.longitude = data.data.longitude;
                    location.latitude = data.data.latitude;
                    location.status = "Deactive";
                    location.tracking_id = data.data._id;
                    trackingService.trackingUpdate(location, "res", (updatedTracker) => {
                        if (updatedTracker.status) {
                            trackingService.getActiveTrackers(obj, "res", (trackers) => {
                                if (trackers.status) {
                                    io.emit('trackers', { type: 'trackers', tracker: trackers.data });
                                } else {
                                    console.log(trackers.data);
                                }
                            });
                        } else {
                            console.log(updatedTracker.data);
                        }
                    });
                })
            }
            else if (data.status) {
                var location = {};
                location.longitude = locationObj.longitude;
                location.latitude = locationObj.latitude;
                location.status = "Active";
                location.tracking_id = data.data._id;
                trackingService.trackingUpdate(location, "res", (updatedTracker) => {
                    if (updatedTracker.status) {
                        trackingService.getActiveTrackers(obj, "res", (trackers) => {
                            if (trackers.status) {
                                io.emit('trackers', { type: 'trackers', tracker: trackers.data });
                            } else {
                                console.log(trackers.data);
                            }
                        });
                    } else {
                        console.log(updatedTracker.data);
                    }
                });
            } else {
                var location = {};
                location.longitude = locationObj.longitude;
                location.latitude = locationObj.latitude;
                location.status = "Active";
                location.bus = locationObj.bus;

                trackingService.startTracking(location, " ", (tracker) => {
                    if (tracker.status) {
                        var journey = {};
                        journey.startTime = locationObj.startTime
                        journey.bus = locationObj.bus;
                        journey.tracker = tracker.data._id;
                        journeyService.startJourney(journey, "", (journeyStatus) => {
                            if (journeyStatus.status) {
                                trackingService.getActiveTrackers(obj, "res", (trackers) => {
                                    if (trackers.status) {
                                        io.emit('trackers', { type: 'trackers', tracker: trackers.data });
                                    } else {
                                        console.log(trackers.data);
                                    }
                                });
                            } else {
                                console.log(journeyStatus);
                            }
                        });
                    } else {
                        console.log(tracker);
                    }
                });
            }
        });
    });

    socket.on('buses', (locationObj) => {
        var obj = {};
        obj.status = "Active";
        trackingService.getActiveTrackers(obj, "res", (trackers) => {
            if (trackers.status) {
                io.emit('trackers', { type: 'trackers', tracker: trackers.data });
            } else {
                console.log(trackers.data);
            }
        });
    });
});

//start server...
http.listen(server_port, err => {
    if (err) {
        console.error(err);
        return;
    }
    console.log('server listening on port : ' + server_port);
});


