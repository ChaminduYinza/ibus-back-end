
module.exports.exportSocket = function (io) {
    io.on('connection', (socket) => {
        console.log('Cordinate Recieved');

        socket.on('disconnect', function () {
            console.log('user disconnected');
        });

        socket.on('add-message', (message) => {
            io.emit('message', { type: 'new-message', text: message });
        });

    });
}
